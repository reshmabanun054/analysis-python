import json
import sys
import os   

src = os.listdir('inputFiles')
for i in src:
    print(i)
    if i != "trimSpace.py":
        print(i)
        if i.endswith(".json"):
            with open("inputFiles/"+i, 'r') as f:
                data_json=json.load(f)
                f.close()
            src1 = os.path.join(os.path.dirname(__file__), "outputFiles", i)
            print("The output path is ", src1)
            with open(src1, 'w') as outfile:
                json.dump(data_json, outfile)
                outfile.close()
        else:
            print('No files are committed yet')




