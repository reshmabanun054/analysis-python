terraform {  
    backend "s3" {
        bucket  = "tf-st-files"
        key     = "terraform.tfstate"    
        region  = "us-east-1"  
    }
}
